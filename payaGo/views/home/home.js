import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  Group,

} from 'react-native';

import { SvgXml } from 'react-native-svg';
import { SvgUri } from 'react-native-svg';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Bell from "../../assets/icons/music.svg"
import Menu from "../../assets/icons/31.svg"
import Arrow from "../../assets/icons/multimedia-option.svg"
import Deposit from "../../assets/icons/business.svg"
import Wallet from "../../assets/icons/e-commerce.svg"
import Transfer from "../../assets/icons/arrows.svg"
import Member from "../../assets/icons/member.svg"
import Withdraw from "../../assets/icons/money.svg"
import P2P from "../../assets/icons/networking.svg"
import Invoice from "../../assets/icons/commerce.svg"
import Secority from "../../assets/icons/surface1.svg"
import Bell2 from "../../assets/icons/Group.svg"
import HomeIcon from "../../assets/icons/social-media.svg"
import History from "../../assets/icons/tools-and-utensils.svg"
import Pay from "../../assets/icons/layer1.svg"
import Balance from "../../assets/icons/tools.svg"
import Chat from "../../assets/icons/communications.svg"
class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        {
          url: require("../../assets/icons/item1.png"),
        },
        {
          url: require("../../assets/icons/item2.jpg"),
        },
        {
          url: require("../../assets/icons/item3.jpg"),
        },
        {
          url: require("../../assets/icons/item4.jpg"),
        },
        {
          url: require("../../assets/icons/item1.png"),
        },
      ],

      activeSlide: 0,
      message: true
    }
  }

  _renderItem({ item, index }, parallaxProps) {
    return (
      <View style={{ width: 320, height: 50, borderRadius: 3, justifyContent: 'center', alignItems: 'center', marginLeft: 28 }}>
        <Image
          style={{ width: '100%', height: 50, resizeMode: 'stretch', borderRadius: 3 }}
          source={item.url}
        />
      </View>
    );
  }
   pagination() {

    return (
      <Pagination
        dotsLength={this.state.items.length}
        activeDotIndex={this.state.activeSlide}
        style={{ height: 15, width: 80, backgroundColor:'yellow'}}
        containerStyle={{ width: 60}}
        dotStyle={styles.dotStyle}
        inactiveDotStyle={styles.inactiveDotStyle}
        inactiveDotOpacity={1}
        inactiveDotScale={0.6}
        activeOpacity={0.5}
        activeDotStyle={styles.activeDotStyle}
      />
    );
  }
  _changeactivekey(id) {
    console.log(this.state.menu);

    let array = this.state.menu
    array.map((el) => {
      el.active = false
    })
    for (let index = 0; index < array.length; index++) {
      if (array[index].id == id) {
        console.log("77777777");

        array[index].active = true
        break
      }
    }
    this.setState({ menu: array })
  }
  _rendermenu() {

    return <View style={{
      flexDirection: 'row',
      justifyContent: 'space-between',
      height: 79.48,
      position: 'absolute',
      top: -15,
      width: 380,
      left: 10,
    }}
    >
      <TouchableOpacity>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <View style={styles.menu_item}>
            <HomeIcon width={25} height={25} />
          </View>
          <Text style={{fontWeight: 'bold',marginTop:-8}}>Home</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <View style={styles.menu_item1}>
            <History width={22.01} height={22.01} />
          </View>
          <Text style={{marginTop:7}}>History</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity >
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <View style={styles.menu_item1}>
            <Pay width={235.92} height={19.95} fill='black' />
          </View>
          <Text style={{marginTop:10}}>Scan Pay</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <View style={styles.menu_item1}>
            <Balance width={27} height={27} fill='black' />
          </View>
          <Text style={{marginTop:3}}>Balance</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <View style={styles.menu_item1}>
            <Chat width={25} height={25} />
          </View>
          <Text style={{marginTop:4}}>Chat</Text>
        </View>
      </TouchableOpacity>
    </View>
  }
  render() {

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.text_header}>PaynGo</Text>
          <View style={styles.icon_view}>
            {this.state.message ? <Bell2 width={21} height={28} /> : <Bell width={30} height={30} />}

            <Menu width={30} height={30}
              fill='black'
            />
          </View>
        </View>


        <View style={styles.body}>

          <View style={{
             width: 332, 
             height: 44,
              backgroundColor: '#01b36d', 
              marginVertical: 10,
               fontSize: 30,
                borderRadius: 3, 
                flexDirection:'row',
                justifyContent:'space-between',
                paddingTop:11,
                paddingLeft:27,
                paddingRight:15.86
                }} >
            <Text
            style={{
              color:'white',
              fontSize:16,
              
            }}
            >E-USD_9000</Text>
                  <Arrow width={14.29} height={8.44}
                    style={{ marginTop:8 }}
                    fill='black'
                  />
          </View>
          <View style={styles.body_col}>
            <View style={styles.col}>
              <TouchableOpacity style={styles.body_TouchableOpacity}>
                <Deposit width={23.47} height={23.47} fill='black' />
                <Text style={{ marginTop: 8.06 }}>Deposit</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.body_TouchableOpacity}>
                <Wallet width={23.96} height={21.91} />
                <Text style={{ marginTop: 8.06 }} >Wallet</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.body_TouchableOpacity}>
                <Transfer width={12} height={24} />
                <Text style={{ marginTop: 8.06 }}>Transfer</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.body_TouchableOpacity}>
                <Member width={25.7} height={22.5} />
                <Text style={{ marginTop: 8.06 }}>Member store</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.col}>
              <TouchableOpacity style={styles.body_TouchableOpacity}>
                <Withdraw width={24.39} height={24.39} />

                <Text style={{ marginTop: 8.06 }}>Withdraw</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.body_TouchableOpacity}>
                <P2P width={49.74} height={21} />
                <Text style={{ marginTop: 8.06 }}>P2P</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.body_TouchableOpacity}>
                <Invoice width={23.34} height={24.89} />
                <Text style={{ marginTop: 8.06 }}>Invoice</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.body_TouchableOpacity}>
                <Secority width={21.29} height={25} />
                <Text style={{ marginTop: 8.06 }}>Secority/Verfication </Text>
              </TouchableOpacity>
            </View>

          </View>
          <View style={{ height: 80, margin: 10, flexDirection:'column', justifyContent: 'center', alignItems: 'center'}}>
            <Carousel
              sliderWidth={380}
              sliderHeight={50}
              itemWidth={380}
              data={this.state.items}
              renderItem={this._renderItem}
              style={{ borderRadius: 3 }}
              onSnapToItem={(index) => this.setState({ activeSlide: index })}

            >

            </Carousel>
            <View style={{height: 15, justifyContent: 'center', alignItems: 'center'}}>
              { this.pagination()}
            </View>
          </View>
        </View>

        <View style={{ backgroundColor: 'white', height: 75, width:'100%', marginTop: 10, position:"absolute", bottom:0 }}>
          {this._rendermenu()}
        </View>
      </View>
    )

  }
}

export default Home
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f2f2f2',
	height:"100%",
	width:"100%"
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 55,
    backgroundColor: 'white'
  },
  text_header: {
    fontSize: 25,
    fontWeight: 'bold',
    marginLeft: 14,
    marginTop: 7
  },
  icon_view: {
    flexDirection: 'row',
    marginTop: 10,
    marginRight: 17,
    width: 75,
    justifyContent: 'space-between'
  },
  body: {
	width: '100%',
	flex:1,
	flexDirection:'column',
    justifyContent: 'space-between',
	alignItems: 'center',
	marginBottom:75
  },
  body_col: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  body_TouchableOpacity: {
    backgroundColor: 'white',
    height: 75,
    width: 160,
    marginHorizontal: 7.5,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 12,
    marginVertical: 4
  },
  col: {
    justifyContent: 'space-between',
    flexDirection: 'column'
  },
  dotStyle: {
    width: 15,
    height: 15,
    borderRadius: 15,
    backgroundColor: 'rgba(255, 255, 255, 0.92)',
    borderColor: '#01b36d',
    borderWidth: 1,
    //marginTop: -26,
    marginHorizontal: 20,
  },
  inactiveDotStyle: {
    backgroundColor: '#01b36d',
    width: 20,
    height: 20,
    borderRadius: 20,
    marginHorizontal: 20,
    marginTop: 5
  },
  activeDotStyle: {
    width: 15,
    height: 15,
    borderRadius: 15,
  },
  menu_item: {
    backgroundColor: '#01B36D',
    borderRadius: 60,
    height: 65,
    width: 65,
    borderWidth: 9,
    borderColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  menu_item1: {
    width: 60,
   marginTop:28,
    alignItems: 'center',
    justifyContent: 'center'
  }


})